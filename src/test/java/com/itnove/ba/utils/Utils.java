package com.itnove.ba.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by guillemhernandezsola on 14/02/2017.
 */
public class Utils {

    public static String getCurrentPath() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + s);
        return s;
    }
}
