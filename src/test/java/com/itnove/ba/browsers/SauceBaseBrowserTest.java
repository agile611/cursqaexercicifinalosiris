package com.itnove.ba.browsers;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileBrowserType;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by guillemhs on 2017-02-16.
 */
public class SauceBaseBrowserTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;

    @Before
    public void setUp() throws MalformedURLException {
        String device = System.getProperty("device");
        // switch between diffrent browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("android5.1")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", "Android Emulator");
            capabilities.setCapability("platformVersion", "5.1");
            capabilities.setCapability("browserName", MobileBrowserType.BROWSER);
            driver = new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), capabilities);
        } else if(device != null && device.equalsIgnoreCase("s4")) {
            DesiredCapabilities caps = DesiredCapabilities.android();
            caps.setCapability("appiumVersion", "1.6.4");
            caps.setCapability("deviceName","Samsung Galaxy S4 GoogleAPI Emulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("browserName", "Browser");
            caps.setCapability("platformVersion", "4.4");
            caps.setCapability("platformName","Android");
            driver = new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        } else if(device != null && device.equalsIgnoreCase("android7")) {
            DesiredCapabilities caps = DesiredCapabilities.android();
            caps.setCapability("appiumVersion", "1.6.4");
            caps.setCapability("deviceName","Android GoogleAPI Emulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("browserName", "Chrome");
            caps.setCapability("platformVersion", "7.0");
            caps.setCapability("platformName","Android");
            driver = new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        }
        else if (device != null && device.equalsIgnoreCase("iphoneX")) {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName","iPhone X Simulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion","11.0");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserdName", "Safari");
            driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        } else if (device != null && device.equalsIgnoreCase("iphone5s")) {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName","iPhone 5s Simulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion","9.3");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "Safari");
            driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        } else if (device != null && device.equalsIgnoreCase("safari")) {
            DesiredCapabilities caps = DesiredCapabilities.safari();
            caps.setCapability("platform", "macOS 10.12");
            caps.setCapability("version", "10.1");
            driver = new RemoteWebDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        } else if (device != null && device.equalsIgnoreCase("edge")) {
            DesiredCapabilities caps = DesiredCapabilities.edge();
            caps.setCapability("platform", "Windows 10");
            caps.setCapability("version", "14.14393");
            driver = new RemoteWebDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        }
        else {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName","iPhone 6 Plus Simulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion","10.0");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "Safari");
            driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        }
        wait = new WebDriverWait(driver, 10);
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}