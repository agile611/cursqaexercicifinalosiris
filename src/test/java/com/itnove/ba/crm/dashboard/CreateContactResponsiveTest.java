package com.itnove.ba.crm.dashboard;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.CreateAccountPageResponsive;
import com.itnove.ba.crm.pages.CreateContactPageResponsive;
import com.itnove.ba.crm.pages.LoginPage;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class CreateContactResponsiveTest extends BaseSauceBrowserTest
{
    public RemoteWebDriver driver;

    @Test
    public void testApp() throws InterruptedException
    {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        CreateContactPageResponsive createContactPageResponsive = new CreateContactPageResponsive(driver);
        createContactPageResponsive.CreateContact("Mr.", "Osiris", "Perez", "123456789",
                "123456789", "MyTitle", "MyDpto", "MyAccount", "123456789",
                "osiris.perez@gmail.com", "Sants", "Barcelona", "Barcelona",
                "08014", "España", "Administrator", "Osiris");
    }
}
