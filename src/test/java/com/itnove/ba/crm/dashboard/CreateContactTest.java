package com.itnove.ba.crm.dashboard;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.CreateContactPage;
import com.itnove.ba.crm.pages.CreateDocumentPage;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.testng.annotations.Test;

import java.io.File;
import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class CreateContactTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        String name = UUID.randomUUID().toString();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.createButtonClick(hover);
        CreateContactPage createContactPage = new CreateContactPage(driver);
        createContactPage.CreateContact("Mr.", "Osiris", "Perez", "123456789", "123456789", "MiTitulo", "MyDpto", "Osiris", "123456789", "osiris.perez@gmail.com","Sants", "Barcelona", "Barcelona", "08014", "Espana", "Employee");
     }
}
