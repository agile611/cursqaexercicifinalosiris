package com.itnove.ba.crm.dashboard;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.crm.pages.CreateDocumentPage;
import com.itnove.ba.crm.pages.CreateTaskPageResponsive;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.io.File;
import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class CreateTaskResponsiveTest extends BaseSauceBrowserTest
{
    public Actions hover;

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        String name = UUID.randomUUID().toString();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.createButtonClick(hover);
        dashboardPage.clickOnCreateDocumentLink(hover);
        CreateTaskPageResponsive createTaskPageResponsive = new CreateTaskPageResponsive(driver);
        createTaskPageResponsive.createTask("OsirisTask","Not Stared","2017-12-11","09","00","Account","Osiris","2017-12-20","17","00","Osiris Perez","High");
     }
}
