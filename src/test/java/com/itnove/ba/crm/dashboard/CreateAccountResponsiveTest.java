package com.itnove.ba.crm.dashboard;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.CreateAccountPageResponsive;
import com.itnove.ba.crm.pages.CreateDocumentPage;
import com.itnove.ba.crm.pages.DashboardPage;
import com.itnove.ba.crm.pages.LoginPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.io.File;
import java.util.UUID;

import static org.testng.Assert.assertTrue;

public class CreateAccountResponsiveTest extends BaseSauceBrowserTest
{

    @Test
    public void testApp() throws InterruptedException
    {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        CreateAccountPageResponsive createAccountPageResponsive = new CreateAccountPageResponsive(driver);
        createAccountPageResponsive.createAccount("osiris", "123456789", "osiris.perez@gmail.com",
                "Sants", "Barcelona", "Barcelona", "08014", "España");

    }
}
