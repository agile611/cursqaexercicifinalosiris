package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateContactPageResponsive
{
    private WebDriver driver;
    private WebDriverWait wait;
    public Actions hover;

    @FindBy(xpath = "id('ajaxHeader')/nav[1]/div[1]/div[1]/button[1]")
    public WebElement menu;

    @FindBy(xpath = "id('mobile_menu')/li[3]/a[1]")
    public WebElement tagContact;

    @FindBy(xpath = "id('content')/div[1]/h2[1]")
    public WebElement contactPage;

    @FindBy(xpath = "id('moduleTab_Contacts')")
    public WebElement moduleContact;

    @FindBy(xpath = "id('modulelinks')/ul[1]/ul[1]/li[1]/a[1]")
    public WebElement createContact;

    @FindBy(xpath = "id('salutation')")
    public WebElement salutation;

    @FindBy(xpath = "id('first_name')")
    public WebElement firstName;

    @FindBy(xpath = "id('last_name')")
    public WebElement lastName;

    @FindBy(xpath = "id('phone_work')")
    public WebElement phoneWork;

    @FindBy(xpath = "id('phone_mobile')")
    public WebElement phoneMobile;

    @FindBy(xpath = "id('title')")
    public WebElement title;

    @FindBy(xpath = "id('department')")
    public WebElement department;

    @FindBy(xpath = "id('account_name')")
    public WebElement accountName;

    @FindBy(xpath = "id('phone_fax')")
    public WebElement phoneFax;

    @FindBy(xpath = "id('Contacts0emailAddress0')")
    public WebElement contactEmailAddress;

    @FindBy(xpath = "id('primary_address_street')")
    public WebElement pAddressStreet;

    @FindBy(xpath = "id('alt_address_street')")
    public WebElement aAddressStreet;

    @FindBy(xpath = "id('primary_address_city')")
    public WebElement pAddressCity;

    @FindBy(xpath = "id('alt_address_city')")
    public WebElement aAddressCity;

    @FindBy(xpath = "id('primary_address_state')")
    public WebElement pAddressState;

    @FindBy(xpath = "id('alt_address_state')")
    public WebElement aAddressState;

    @FindBy(xpath = "id('primary_address_postalcode')")
    public WebElement pAddressPC;

    @FindBy(xpath = "id('alt_address_postalcode')")
    public WebElement aAddressPC;

    @FindBy(xpath = "id('primary_address_country')")
    public WebElement pAddressCountry;

    @FindBy(xpath = "id('alt_address_country')")
    public WebElement aAddressCountry;

    @FindBy(xpath = "id('assigned_user_name')")
    public WebElement assignedTo;

    @FindBy(xpath = "id('lead_source')")
    public WebElement leadSource;

    @FindBy(xpath = "id('SAVE')")
    public WebElement btnSave;

    @FindBy (xpath = "id('pagecontent')/div[1]")
    public WebElement contactCreate;


    public void CreateContact(String salute, String firstname, String lastname, String phonework, String phonemobile, String titulo, String departamento,
                              String accountname, String phonefax, String contactemailaddress, String paddressstreet, String paddresscity,
                              String paddressstate, String paddresspc, String paddresscountry, String assignedto, String leadsource)
    {
        // muevo el mouse hasta el menú para darle click
        hover.moveToElement(menu).moveToElement(menu).click().build().perform();
        menu.click();
        //muevo el mouse por el menu hasta Contact y accedo a la página de contactos
        hover.moveToElement(tagContact).moveToElement(tagContact).click().build().perform();
        tagContact.click();
        //espero a que carge la página de contactos
        wait.until(ExpectedConditions.visibilityOf(contactPage));
        //muevo el mouse al menu del módulo contactos y doy click
        hover.moveToElement(moduleContact).moveToElement(moduleContact).click().build().perform();
        moduleContact.click();
        //muevo el mouse por el menu de contactos y doy click en crear contacto
        hover.moveToElement(createContact).moveToElement(createContact).click().build().perform();
        createContact.click();
        //espero a que carge la página de crear contactos
        wait.until(ExpectedConditions.visibilityOf(contactPage));

        salutation.sendKeys(salute);
        firstName.click();
        firstName.clear();
        firstName.sendKeys(firstname);
        lastName.click();
        lastName.clear();
        lastName.sendKeys(lastname);
        phoneWork.click();
        phoneWork.clear();
        phoneWork.sendKeys(phonework);
        phoneMobile.click();
        phoneMobile.clear();
        phoneMobile.sendKeys(phonemobile);
        title.click();
        title.clear();
        title.sendKeys(titulo);
        department.click();
        department.clear();
        department.sendKeys(departamento);
        accountName.click();
        accountName.clear();
        accountName.sendKeys(accountname);
        phoneFax.click();
        phoneFax.clear();
        phoneFax.sendKeys(phonefax);
        contactEmailAddress.click();
        contactEmailAddress.clear();
        contactEmailAddress.sendKeys(contactemailaddress);
        pAddressStreet.click();
        pAddressStreet.clear();
        pAddressStreet.sendKeys(paddressstreet);
        aAddressStreet.click();
        aAddressStreet.clear();
        aAddressStreet.sendKeys(paddressstreet);
        pAddressCity.click();
        pAddressCity.clear();
        pAddressCity.sendKeys(paddresscity);
        aAddressCity.click();
        aAddressCity.clear();
        aAddressCity.sendKeys(paddresscity);
        pAddressState.click();
        pAddressState.clear();
        pAddressState.sendKeys(paddressstate);
        aAddressState.click();
        aAddressState.clear();
        aAddressState.sendKeys(paddressstate);
        pAddressPC.click();
        pAddressPC.clear();
        pAddressPC.sendKeys(paddresspc);
        aAddressPC.click();
        aAddressPC.clear();
        aAddressPC.sendKeys(paddresspc);
        pAddressCountry.click();
        pAddressCountry.clear();
        pAddressCountry.sendKeys(paddresscountry);
        aAddressCountry.click();
        aAddressCountry.clear();
        aAddressCountry.sendKeys(paddresscountry);
        assignedTo.clear();
        assignedTo.sendKeys(assignedto);
        leadSource.click();
        leadSource.clear();
        leadSource.sendKeys(leadsource);
        btnSave.click();
    }

    public boolean iscontactCreate(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(contactCreate));
        return contactCreate.isDisplayed();
    }

    public CreateContactPageResponsive(WebDriver driver)
    {
        PageFactory.initElements(driver, this);
    }

}
