package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateContactPage
{
    private WebDriver driver;
    private WebDriverWait wait;
    public Actions hover;

    @FindBy(xpath = "id('grouptab_0')")
    public WebElement tagSales;

    @FindBy(xpath = "id('toolbar')/ul[1]/li[2]/span[2]/ul[1]/li[3]")
    public WebElement tagSalesContact;

    @FindBy(xpath = "id('content')/div[1]")
    public WebElement contactPage;

    @FindBy(xpath = "id('actionMenuSidebar')/ul[1]/li[1]/a[1]/div[2]")
    public WebElement createContact;

    @FindBy(xpath = "id('salutation')")
    public WebElement salutation;

    @FindBy(xpath = "id('first_name')")
    public WebElement firstName;

    @FindBy(xpath = "id('last_name')")
    public WebElement lastName;

    @FindBy(xpath = "id('phone_work')")
    public WebElement phoneWork;

    @FindBy(xpath = "id('phone_mobile')")
    public WebElement phoneMobile;

    @FindBy(xpath = "id('title')")
    public WebElement title;

    @FindBy(xpath = "id('department')")
    public WebElement department;

    @FindBy(xpath = "id('account_name')")
    public WebElement accountName;

    @FindBy(xpath = "id('phone_fax')")
    public WebElement phoneFax;

    @FindBy(xpath = "id('Contacts0emailAddress0')")
    public WebElement contactEmailAddress;

    @FindBy(xpath = "id('primary_address_street')")
    public WebElement pAddressStreet;

    @FindBy(xpath = "id('alt_address_street')")
    public WebElement aAddressStreet;

    @FindBy(xpath = "id('primary_address_city')")
    public WebElement pAddressCity;

    @FindBy(xpath = "id('alt_address_city')")
    public WebElement aAddressCity;

    @FindBy(xpath = "id('primary_address_state')")
    public WebElement pAddressState;

    @FindBy(xpath = "id('alt_address_state')")
    public WebElement aAddressState;

    @FindBy(xpath = "id('primary_address_postalcode')")
    public WebElement pAddressPC;

    @FindBy(xpath = "id('alt_address_postalcode')")
    public WebElement aAddressPC;

    @FindBy(xpath = "id('primary_address_country')")
    public WebElement pAddressCountry;

    @FindBy(xpath = "id('alt_address_country')")
    public WebElement aAddressCountry;

    @FindBy(xpath = "id('lead_source')")
    public WebElement leadSource;

    @FindBy(xpath = "id('SAVE')")
    public WebElement btnSave;

    @FindBy (xpath = "id('pagecontent')/div[1]")
    public WebElement contactCreate;


    public void CreateContact(String salute, String firstname, String lastname, String phonework, String phonemobile, String titulo, String departamento,
                              String accountname, String phonefax, String contactemailaddress, String paddressstreet, String paddresscity, String paddressstate,
                              String paddresspc, String paddresscountry, String leadsource)
    {
        hover.moveToElement(tagSales).moveToElement(tagSales).click().build().perform();
        tagSales.click();
        tagSalesContact.click();
        wait.until(ExpectedConditions.visibilityOf(contactPage));
        createContact.click();
        salutation.sendKeys(salute);
        firstName.click();
        firstName.clear();
        firstName.sendKeys(firstname);
        lastName.click();
        lastName.clear();
        lastName.sendKeys(lastname);
        phoneWork.click();
        phoneWork.clear();
        phoneWork.sendKeys(phonework);
        phoneMobile.click();
        phoneMobile.clear();
        phoneMobile.sendKeys(phonemobile);
        title.click();
        title.clear();
        title.sendKeys(titulo);
        department.click();
        department.clear();
        department.sendKeys(departamento);
        accountName.click();
        accountName.clear();
        accountName.sendKeys(accountname);
        phoneFax.click();
        phoneFax.clear();
        phoneFax.sendKeys(phonefax);
        contactEmailAddress.click();
        contactEmailAddress.clear();
        contactEmailAddress.sendKeys(contactemailaddress);
        pAddressStreet.click();
        pAddressStreet.clear();
        pAddressStreet.sendKeys(paddressstreet);
        aAddressStreet.click();
        aAddressStreet.clear();
        aAddressStreet.sendKeys(paddressstreet);
        pAddressCity.click();
        pAddressCity.clear();
        pAddressCity.sendKeys(paddresscity);
        aAddressCity.click();
        aAddressCity.clear();
        aAddressCity.sendKeys(paddresscity);
        pAddressState.click();
        pAddressState.clear();
        pAddressState.sendKeys(paddressstate);
        aAddressState.click();
        aAddressState.clear();
        aAddressState.sendKeys(paddressstate);
        pAddressPC.click();
        pAddressPC.clear();
        pAddressPC.sendKeys(paddresspc);
        aAddressPC.click();
        aAddressPC.clear();
        aAddressPC.sendKeys(paddresspc);
        pAddressCountry.click();
        pAddressCountry.clear();
        pAddressCountry.sendKeys(paddresscountry);
        aAddressCountry.click();
        aAddressCountry.clear();
        aAddressCountry.sendKeys(paddresscountry);
        leadSource.click();
        leadSource.clear();
        leadSource.sendKeys(leadsource);
        btnSave.click();
    }

    public boolean iscontactCreate(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(contactCreate));
        return contactCreate.isDisplayed();
    }

    public CreateContactPage(WebDriver driver)
    {
        PageFactory.initElements(driver, this);
    }

}
