package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateAccountPageResponsive
{

    private WebDriver driver;
    private Actions hover;
    private WebDriverWait wait;

    @FindBy(xpath = "id('quickcreatetop')/a[1]")
    public WebElement tagCreate;

    @FindBy(xpath = "id('quickcreatetop')/ul[1]/li[1]/a[1]")
    public WebElement tagCreateAccount;

    @FindBy(xpath = "id('pagecontent')/div[1]/h2[1]")
    public WebElement pageCreateAccount;

    @FindBy(id = "name")
    public WebElement nameTextbox;

    @FindBy(id = "phone_office")
    public WebElement phoneOffice;

    @FindBy(id = "Accounts0emailAddress0")
    public WebElement accountEmail;

    @FindBy(id = "billing_address_street")
    public WebElement addresstreet;

    @FindBy(id = "billing_address_city")
    public WebElement addresscity;

    @FindBy(id = "billing_address_state")
    public WebElement addresstate;

    @FindBy(id = "billing_address_postalcode")
    public WebElement addresspostalcode;

    @FindBy(id = "billing_address_country")
    public WebElement addresscountry;

    @FindBy(id = "shipping_checkbox")
    public WebElement shipping;

    @FindBy(id = "SAVE")
    public WebElement saveButton;

    @FindBy(id = "xstab0")
    public WebElement creado;

    public void createAccount(String name, String phono, String email, String street, String city, String state, String codepostal,
                              String country)
    {
        hover.moveToElement(tagCreate).moveToElement(tagCreate).click().build().perform();
        tagCreate.click();
        hover.moveToElement(tagCreateAccount).moveToElement(tagCreateAccount).click().build().perform();
        tagCreateAccount.click();
        wait.until(ExpectedConditions.visibilityOf(pageCreateAccount));

        nameTextbox.click();
        nameTextbox.clear();
        nameTextbox.sendKeys(name);
        phoneOffice.click();
        phoneOffice.clear();
        phoneOffice.sendKeys(phono);
        accountEmail.click();
        accountEmail.clear();
        accountEmail.sendKeys(email);
        addresstreet.click();
        addresstreet.clear();
        addresstreet.sendKeys(street);
        addresscity.click();
        addresscity.clear();
        addresscity.sendKeys(city);
        addresstate.click();
        addresstate.clear();
        addresstate.sendKeys(state);
        addresspostalcode.click();
        addresspostalcode.clear();
        addresspostalcode.sendKeys(codepostal);
        addresscountry.click();
        addresscountry.clear();
        addresscountry.sendKeys(country);
        shipping.click();

        saveButton.click();
    }

    public boolean isaccountCreate(WebDriver driver, WebDriverWait wait)
    {
        wait.until(ExpectedConditions.visibilityOf(creado));
        return creado.isDisplayed();
    }

    public CreateAccountPageResponsive(WebDriver driver)
    {
        PageFactory.initElements(driver, this);
    }
}