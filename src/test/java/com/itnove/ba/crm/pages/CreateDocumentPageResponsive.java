package com.itnove.ba.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateDocumentPageResponsive {

    private WebDriver driver;
    private Actions hover;
    private WebDriverWait wait;

    @FindBy(xpath = "id('quickcreatetop')/a[1]")
    public WebElement tagCreate;

    @FindBy(xpath = "id('quickcreatetop')/ul[1]/li[5]/a[1]")
    public WebElement tagCreateDoc;

    @FindBy(xpath = "id('pagecontent')/div[1]/h2[1]")
    public WebElement pageCreateDc;

    @FindBy(xpath = "id('status_id')")
    public WebElement status;

    @FindBy(id = "SAVE")
    public WebElement btnSave;

    @FindBy(xpath = "id('pagecontent')/div[1]")
    public WebElement docCreated;


    public void createDocument(String estado)
    {
        hover.moveToElement(tagCreate).moveToElement(tagCreate).click().build().perform();
        tagCreate.click();
        hover.moveToElement(tagCreateDoc).moveToElement(tagCreateDoc).click().build().perform();
        tagCreateDoc.click();

        wait.until(ExpectedConditions.visibilityOf(pageCreateDc));
        WebElement fileUpload = driver.findElement(By.id("file-upload"));
        File file = new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "documento_de_prueba.txt");
        fileUpload.sendKeys(file.getAbsolutePath());

        status.sendKeys(estado);
        btnSave.click();
    }

    public boolean isdocumentCreate(WebDriver driver, WebDriverWait wait)
    {
        wait.until(ExpectedConditions.visibilityOf(docCreated));
        return docCreated.isDisplayed();
    }

    public CreateDocumentPageResponsive(WebDriver driver)
    {
        PageFactory.initElements(driver, this);
    }

}
