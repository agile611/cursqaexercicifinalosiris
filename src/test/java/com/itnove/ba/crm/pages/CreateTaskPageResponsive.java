package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateTaskPageResponsive
{

    private WebDriver driver;
    private Actions hover;
    private WebDriverWait wait;

    @FindBy(xpath = "id('quickcreatetop')/a[1]")
    public WebElement tagCreate;

    @FindBy(xpath = "id('quickcreatetop')/ul[1]/li[7]/a[1]")
    public WebElement tagCreateTask;

    @FindBy(xpath = "id('pagecontent')/div[1]/h2[1]")
    public WebElement pageCreateTask;

    @FindBy(id = "name")
    public WebElement nameTask;

    @FindBy(id = "status")
    public WebElement statusTask;

    @FindBy(id = "date_start_date")
    public WebElement dateStartTask;

    @FindBy(id = "date_start_hours")
    public WebElement hourStartTask;

    @FindBy(id = "date_start_minutes")
    public WebElement minuteStartTask;

    @FindBy(id = "parent_type")
    public WebElement relatedTo;

    @FindBy(id = "parent_name")
    public WebElement parentName;

    @FindBy(id = "date_due_date")
    public WebElement dateFinishTask;

    @FindBy(id = "date_due_hours")
    public WebElement hourFinishTask;

    @FindBy(id = "date_due_minutes")
    public WebElement minutesFinishTask;

    @FindBy(id = "contact_name")
    public WebElement contactNameTask;

    @FindBy(id = "priority")
    public WebElement priorityTask;

    @FindBy(id = "SAVE")
    public WebElement saveButton;

    @FindBy(id = "xstab0")
    public WebElement creado;

    public void createTask (String name, String estado, String dateStart, String hourStart, String minutesStart, String related,
                              String nameparent, String dateFinish, String hourFinish, String minutesFinish, String nameContact,
                              String priority)
    {
        hover.moveToElement(tagCreate).moveToElement(tagCreate).click().build().perform();
        tagCreate.click();
        hover.moveToElement(tagCreateTask).moveToElement(tagCreateTask).click().build().perform();
        tagCreateTask.click();
        wait.until(ExpectedConditions.visibilityOf(pageCreateTask));

        nameTask.click();
        nameTask.clear();
        nameTask.sendKeys(name);
        statusTask.click();
        statusTask.clear();
        statusTask.sendKeys(estado);
        dateStartTask.click();
        dateStartTask.clear();
        dateStartTask.sendKeys(dateStart);
        hourStartTask.click();
        hourStartTask.clear();
        hourStartTask.sendKeys(hourStart);
        minuteStartTask.click();
        minuteStartTask.clear();
        minuteStartTask.sendKeys(minutesStart);
        relatedTo.click();
        relatedTo.clear();
        relatedTo.sendKeys(related);
        parentName.click();
        parentName.clear();
        parentName.sendKeys(nameparent);
        dateFinishTask.click();
        dateFinishTask.clear();
        dateFinishTask.sendKeys(dateFinish);
        hourFinishTask.click();
        hourFinishTask.clear();
        hourFinishTask.sendKeys(hourFinish);
        minutesFinishTask.click();
        minutesFinishTask.clear();
        minutesFinishTask.sendKeys(minutesFinish);
        contactNameTask.click();
        contactNameTask.clear();
        contactNameTask.sendKeys(nameContact);
        priorityTask.click();
        priorityTask.clear();
        priorityTask.sendKeys(priority);

        saveButton.click();
    }

    public boolean isaccountCreate(WebDriver driver, WebDriverWait wait)
    {
        wait.until(ExpectedConditions.visibilityOf(creado));
        return creado.isDisplayed();
    }

    public CreateTaskPageResponsive(WebDriver driver)
    {
        PageFactory.initElements(driver, this);
    }
}