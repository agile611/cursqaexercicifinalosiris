package com.itnove.ba.opencart.test;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;


/**
 * Created by guillem on 29/02/16.
 */
public class LogOutCorrecteTest extends BaseTest {
    private WebDriver driver;
    @FindBy(xpath = "id('top-links')/ul[1]/li[2]/ul[1]/li[5]/a[1]")
    public WebElement linklogout;


    @Test
    public void logouTest()
    {
        driver.navigate().to("http://opencart.votarem.lu/");
        LoginPage loginPage = new LoginPage(driver);
        String email = "osiris.perez@gmail.com";
        String pass = "yamilerkis";
        loginPage.login(email, pass);
        loginPage.userBottom.click();
        linklogout.click();
   }
}
