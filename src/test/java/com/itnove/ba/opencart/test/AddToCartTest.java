package com.itnove.ba.opencart.test;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pages.SearchResultsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

/**
 * Created by guillem on 01/03/16.
 */
public class AddToCartTest extends BaseTest {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "id('button-cart')")
    public WebElement buttonAddCart;

    @FindBy(xpath = "id('product-product')/div[1]")
    public WebElement addCartSuccess;

    @Test
    public void addToCart () throws InterruptedException
    {
        driver.navigate().to("http://opencart.votarem.lu/");
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        searchResultsPage.search("mac");
        buttonAddCart.click();
        wait.until(ExpectedConditions.visibilityOf(addCartSuccess));
        addCartSuccess.isDisplayed();
    }
}
