package com.itnove.ba.opencart.test;

import com.itnove.ba.BaseSauceBrowserTest;
import com.itnove.ba.opencart.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

/**
 * Created by guillem on 01/03/16.
 */
public class CheckOutTest extends BaseSauceBrowserTest {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "id('top-links')/ul[1]/li[4]/a[1]/i[1]")
    public WebElement linkCheckOut;

    @FindBy(xpath = "id('content')/div[3]/div[2]/a[1]")
    public WebElement buttonCheckOut;

    @FindBy(xpath = "id('input-payment-firstname')")
    public WebElement paymentFirstName;

    @FindBy(xpath = "id('input-payment-lastname')")
    public WebElement paymentLastName;

    @FindBy(xpath = "id('input-payment-address-1')")
    public WebElement paymentAddress;

    @FindBy(xpath = "id('input-payment-city')")
    public WebElement paymentCity;

    @FindBy(xpath = "id('input-payment-country')")
    public WebElement paymentCountry;

    @FindBy(xpath = "id('input-payment-zone')")
    public WebElement paymentZone;

    @FindBy(xpath = "id('button-payment-address')")
    public WebElement paymentBtnAddress;

    @FindBy(xpath = "id('collapse-shipping-address')/div[1]/form[1]/div[1]/label[1]")
    public WebElement selectUseExisting;

    @FindBy(xpath = "id('button-shipping-address')")
    public WebElement btnShippingAddress;

    @FindBy(xpath = "id('collapse-shipping-method')/div[1]/div[1]/label[1]")
    public WebElement selectShippingMeth;

    @FindBy(xpath = "id('button-shipping-method')")
    public WebElement btnShippingMeth;

    @FindBy(xpath = "id('collapse-payment-method')/div[1]/div[1]")
    public WebElement selectPayMeth;

    @FindBy(xpath = "id('collapse-payment-method')/div[1]/div[2]/div[1]/input[1]")
    public WebElement boxPayMeth;

    @FindBy(xpath = "id('button-payment-method')")
    public WebElement btnPayMeth;

    @FindBy(xpath = "id('button-confirm')")
    public WebElement btnConfirm;

    @FindBy(xpath = "id('common-success')/ul[1]")
    public WebElement success;

    @Test
    public void CheckOut ()
    {
        driver.navigate().to("http://opencart.votarem.lu/");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("osiris.perez@gmail.com", "yamilerkis");

        linkCheckOut.click();
        buttonCheckOut.click();
        paymentFirstName.click();
        paymentFirstName.clear();
        paymentFirstName.sendKeys("Osiris");
        paymentLastName.click();
        paymentLastName.clear();
        paymentLastName.sendKeys("Perez");
        paymentAddress.click();
        paymentAddress.clear();
        paymentAddress.sendKeys("Sants 28");
        paymentCity.click();
        paymentCity.clear();
        paymentCity.sendKeys("Barcelona");
        paymentCountry.sendKeys("Spain");
        paymentZone.sendKeys("Barcelona");
        paymentBtnAddress.click();
        selectUseExisting.isSelected();
        btnShippingAddress.click();

        selectShippingMeth.isSelected();
        btnShippingMeth.click();

        selectPayMeth.isSelected();
        boxPayMeth.click();
        btnPayMeth.click();

        btnConfirm.click();
    }

    public boolean isSuccess ()
    {
       if (success.getText().contains("Success"))
           return true;
       else
           return false;
    }
}
