package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPageResponsive {

    private WebDriver driver;

    @FindBy(xpath = "id('top-links')/ul[1]/li[2]/a[1]")
    public WebElement userBottom;

    @FindBy(xpath = "id('top-links')/ul[1]/li[2]/ul[1]/li[2]/a[1]")
    public WebElement linkLogin;

    @FindBy(xpath = "id('input-email')")
    public WebElement sendEmail;

    @FindBy(xpath = "id('input-password')")
    public WebElement sendPass;

    @FindBy(xpath = "id('content')/div[1]/div[2]/div[1]/form[1]/input[1]")
    public WebElement buttomLogin;

    @FindBy(xpath = "id('account-login')/div[1]")
    public WebElement errorMessage;

    @FindBy(xpath = "id('content')/h2[1]")
    public WebElement correcteMessage;

    public void login(String mail, String passwd){
        userBottom.click();
        linkLogin.click();
        sendEmail.clear();
        sendEmail.sendKeys(mail);
        sendPass.clear();
        sendPass.sendKeys(passwd);
        buttomLogin.click();
    }

    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }

    public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(buttomLogin));
        return buttomLogin.isDisplayed();
    }

    public String errorMessageDisplayed(){
        return errorMessage.getText();
    }

    public LoginPageResponsive(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
