package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateAccountPage {

    private WebDriver driver;

    @FindBy(xpath = "id('input-firstname')")
    public WebElement firstNameTextbox;

    @FindBy(xpath = "id('input-lastname')")
    public WebElement lastNameTextbox;

    @FindBy(xpath = "id('input-email')")
    public WebElement emailTextbox;

    @FindBy(xpath = "id('input-telephone')")
    public WebElement telephoneTextbox;

    @FindBy(xpath = "id('input-password')")
    public WebElement passwordTextbox;

    @FindBy(xpath = "id('input-confirm')")
    public WebElement confirmPassTextbox;

    @FindBy(xpath = "id('content')/form[1]/div[1]/div[1]/input[1]")
    public WebElement privacyPolicy;

    @FindBy(xpath = "id('content')/form[1]/div[1]/div[1]/input[2]")
    public WebElement bottomContinue;

    @FindBy(xpath = "id('content')/p[1]")
    public WebElement msgCongratulation;

    public void createAccount(String name, String lastName, String email, String phono, String pass)
    {
       driver.navigate().to("http://opencart.votarem.lu");
       firstNameTextbox.click();
       firstNameTextbox.clear();
       firstNameTextbox.sendKeys(name);
       lastNameTextbox.click();
       lastNameTextbox.clear();
       lastNameTextbox.sendKeys(lastName);
       emailTextbox.click();
       emailTextbox.clear();
       emailTextbox.sendKeys(email);
       telephoneTextbox.click();
       telephoneTextbox.clear();
       telephoneTextbox.sendKeys(phono);
       passwordTextbox.click();
       passwordTextbox.clear();
       passwordTextbox.sendKeys(pass);
       confirmPassTextbox.click();
       confirmPassTextbox.clear();
       confirmPassTextbox.sendKeys(pass);
       privacyPolicy.isSelected();
       bottomContinue.click();
    }

    public boolean isCreateAccount(WebDriver driver, WebDriverWait wait)
    {
        wait.until(ExpectedConditions.visibilityOf(msgCongratulation));
        return msgCongratulation.isDisplayed();
    }

    public CreateAccountPage(WebDriver driver)
    {
        PageFactory.initElements(driver, this);
    }

}
