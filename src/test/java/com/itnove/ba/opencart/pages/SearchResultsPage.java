package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class SearchResultsPage {

    private WebDriver driver;

    @FindBy(xpath = "id('search')/input[1]")
    public WebElement searchForm;

    @FindBy(xpath = "id('search')/span[1]/button[1]")
    public WebElement lupaSearch;

    @FindBy(xpath = "id('product-search')/ul[1]")
    public WebElement searchResults;

    @FindBy(xpath = "id('content')/p[2]")
    public WebElement noResults;

    @FindBy(xpath = "id('content')/div[3]/div[1]")
    public WebElement firstElement;

    public void search (String criterias)
    {
        searchForm.clear();
        searchForm.click();
        searchForm.sendKeys(criterias);
        lupaSearch.click();
    }

    public boolean isSearchResultsPageLoaded(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(searchResults));
        return searchResults.isDisplayed();
    }

    public boolean isNoSearchResultsDisplayed(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(noResults));
        return noResults.isDisplayed();
    }

    public String isSearchKeywordCorrect()
    {
        return searchForm.getAttribute("value");
    }
    public void clickOnFirstResult(WebDriverWait wait) throws InterruptedException {
        firstElement.click();
    }


    public SearchResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
